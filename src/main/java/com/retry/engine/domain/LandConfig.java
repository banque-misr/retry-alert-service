package com.retry.engine.domain;

import com.retry.engine.config.Status;
import lombok.*;

import javax.persistence.*;
import java.time.LocalTime;
import java.time.ZonedDateTime;

/**
 * @author abdulmaroof
 */
@Table(name = "land_config")
@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LandConfig {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "land_id", referencedColumnName = "id")
    private Land land;

    @Column(name = "start_time", nullable = false)
    private LocalTime startTime;

    @Column(name = "water_gallons", nullable = false)
    private Long waterGallons;

    @Column(name = "duration", nullable = false)
    private Long duration;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private Status status;

    @Column(name = "last_update_date", nullable = false)
    private ZonedDateTime lastUpdateDate;

    @Column(name = "retry_count", nullable = false)
    private Long retryCount;

    @Column(name = "crop_type", nullable = false)
    private String cropType;

}
