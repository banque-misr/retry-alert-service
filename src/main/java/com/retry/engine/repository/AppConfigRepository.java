package com.retry.engine.repository;

import com.retry.engine.config.AppConfigKey;
import com.retry.engine.domain.AppConfig;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AppConfigRepository extends JpaRepository<AppConfig, Long> {
    Optional<AppConfig> findByKey(AppConfigKey name);
}
