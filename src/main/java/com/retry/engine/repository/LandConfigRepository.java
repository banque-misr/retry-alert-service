package com.retry.engine.repository;

import com.retry.engine.config.Status;
import com.retry.engine.domain.LandConfig;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LandConfigRepository extends JpaRepository<LandConfig, Long> {

    List<LandConfig> findByStatusInAndRetryCountLessThanEqual(List<Status> status, long retryCount);
}
