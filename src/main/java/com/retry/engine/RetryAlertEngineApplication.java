package com.retry.engine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;


@EnableScheduling
@SpringBootApplication
public class RetryAlertEngineApplication {

    public static void main(String[] args) {
        SpringApplication.run(RetryAlertEngineApplication.class, args);
    }

}
