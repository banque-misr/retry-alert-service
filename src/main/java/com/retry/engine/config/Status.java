package com.retry.engine.config;

public enum Status {
    SCHEDULED,
    COMPLETED,
    FAILED
}
