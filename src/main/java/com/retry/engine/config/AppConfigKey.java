package com.retry.engine.config;

/**
 * @author abdulmaroof
 */
public enum AppConfigKey {
    MAX_RETRY_COUNT
}
