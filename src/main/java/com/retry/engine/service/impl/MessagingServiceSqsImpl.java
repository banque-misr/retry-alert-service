package com.retry.engine.service.impl;

import com.retry.engine.config.AppConfigKey;
import com.retry.engine.domain.LandConfig;
import com.retry.engine.repository.AppConfigRepository;
import com.retry.engine.service.LandConfigService;
import com.retry.engine.service.MessagingService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.aws.messaging.config.annotation.EnableSns;
import org.springframework.cloud.aws.messaging.core.NotificationMessagingTemplate;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import static com.retry.engine.config.Status.COMPLETED;
import static com.retry.engine.config.Status.SCHEDULED;

/**
 * @author abdulmaroof
 */
@EnableSns
@Service
@RequiredArgsConstructor
public class MessagingServiceSqsImpl implements MessagingService {

    private static final Logger logger = LoggerFactory.getLogger(MessagingServiceSqsImpl.class);

    private final NotificationMessagingTemplate notificationMessagingTemplate;

    private final LandConfigService landConfigService;

    private final AppConfigRepository appConfigRepository;

    private final QueueMessagingTemplate messagingTemplate;


    @Value("${cloud.aws.retry-sqs.end-point}")
    private String sqsEndpoint;

    @Value("${cloud.aws.sns-topic.name}")
    private String topicName;

    @Value("${job.schedule.retry-enabled}")
    private boolean enabled;

    @Scheduled(cron = "${job.schedule.retry-cron}")
    public void sendMessage() {
        if (!enabled)
            return;

        Long retryCount = Long.parseLong(this.appConfigRepository.findByKey(AppConfigKey.MAX_RETRY_COUNT).orElseThrow(() -> new NoSuchElementException("No such element exists")).getValue());
        Map<String, Object> headers = new HashMap<>();
        List<LandConfig> landConfigList = this.landConfigService.fetchFailedAndCompletedNotifications();
        if (!landConfigList.isEmpty())
            landConfigList.forEach(config -> {
                if (COMPLETED.equals(config.getStatus()))
                    this.landConfigService.save(restStatus(config));
                else {
                    if (config.getRetryCount().equals(retryCount))
                        this.raiseAlert(config);
                    else {
                        logger.info("Pushing to queue for retry config with id: {}", config.getId());
                        config.setRetryCount(config.getRetryCount() + 1);
                        messagingTemplate.convertAndSend(sqsEndpoint, MessageBuilder.withPayload(config).build(), headers);
                    }
                }
            });
    }

    private void raiseAlert(LandConfig landConfig) {
        String message = String.format("Maximum retries done for config id %s with land id %s", landConfig.getId(), landConfig.getLand().getId());
        logger.info("Raising Alert message: {}", message);
        this.notificationMessagingTemplate.sendNotification(topicName, message);
        this.landConfigService.save(restStatus(landConfig));
    }

    private LandConfig restStatus(LandConfig landConfig) {
        landConfig.setStatus(SCHEDULED);
        landConfig.setRetryCount(0L);
        return landConfig;
    }
}
