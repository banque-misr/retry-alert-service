package com.retry.engine.service.impl;

import com.retry.engine.domain.LandConfig;
import com.retry.engine.repository.AppConfigRepository;
import com.retry.engine.repository.LandConfigRepository;
import com.retry.engine.service.LandConfigService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;

import static com.retry.engine.config.AppConfigKey.MAX_RETRY_COUNT;
import static com.retry.engine.config.Status.COMPLETED;
import static com.retry.engine.config.Status.FAILED;

/**
 * @author abdulmaroof
 */

@Service
@AllArgsConstructor
@Transactional
public class LandConfigServiceImpl implements LandConfigService {

    private static final Logger logger = LoggerFactory.getLogger(LandConfigServiceImpl.class);

    private final LandConfigRepository configRepository;

    private final AppConfigRepository appConfigRepository;

    @Override
    public List<LandConfig> fetchFailedAndCompletedNotifications() {
        return this.configRepository.findByStatusInAndRetryCountLessThanEqual(Arrays.asList(FAILED, COMPLETED), Long.parseLong(this.appConfigRepository.findByKey(MAX_RETRY_COUNT).get().getValue()));
    }

    @Override
    public void save(LandConfig config) {
        this.configRepository.save(config);
    }

}
