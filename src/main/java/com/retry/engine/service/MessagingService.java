package com.retry.engine.service;

public interface MessagingService {
    void sendMessage();
}
