package com.retry.engine.service;

import com.retry.engine.domain.LandConfig;

import java.util.List;

public interface LandConfigService {

    List<LandConfig> fetchFailedAndCompletedNotifications();

    void save(LandConfig config);
}
